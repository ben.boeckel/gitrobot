#=============================================================================
# Copyright 2010-2016 Kitware, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#=============================================================================
import email.utils
import dns.resolver
import re
from Utility import *

default_domains = [
    ]

class CheckPerson:
    def __init__(self, domains = [], only = False):
        self._known = {}
        self._only = only
        for domain in domains:
            self._known[domain] = True
        if not self._only:
            for domain in default_domains:
                self._known[domain] = True
        self._person = re.compile('^[^@<]+ <[^@]+(@[^@]+)?>')
        self._instructions = """Run
  git config --global user.name 'Your Name'
  git config --global user.email 'you@yourdomain.com'
before creating commits."""
    def _check_person(self, commit, role, person):
        if not self._person.match(person):
            fail("""commit %s has unknown %s field format:
  %s
%s""" % (commit.sha1[0:8], role, person, self._instructions))

        # Parse out the name and email address.
        name, addr = email.utils.parseaddr(person)
        if '@' in addr:
            user, domain = addr.rsplit('@', 1)
        else:
            user, domain = addr, ''
        #die('name=[%s], user=[%s], domain=[%s]' % (name, user, domain))
        if not self._known.has_key(domain):
            self._known[domain] = False
            if not self._only and len(domain):
                try:
                    dns.resolver.query(domain, 'MX')
                    self._known[domain] = True
                except:
                    try:
                        dns.resolver.query(domain)
                        self._known[domain] = True
                    except: pass
        if not self._known[domain]:
            fail('commit %s has unknown %s domain "%s".  %s' % (commit.sha1[0:8], role, domain, self._instructions))
        if name != 'GitHub' and not ' ' in name:
            fail('commit %s has %s name "%s" with no space.  %s' % (commit.sha1[0:8], role, name, self._instructions))
    def enforce(self, commit, user):
        self._check_person(commit, 'author', commit.author)
        self._check_person(commit, 'committer', commit.committer)
