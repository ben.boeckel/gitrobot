#=============================================================================
# Copyright 2010-2016 Kitware, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#=============================================================================
import re
from Utility import *

class CheckCRLF:
    def __init__(self):
        self._parse_attr = re.compile(r'^(?P<path>[^:]*): (?P<attr>[^:]*): (?P<info>.*)$')

    def _check_crlf(self, commit):
        failures = []
        # Reject CRLF line endings added by patch.
        crlf = re.compile(r'^\+[^\n]*\r$', flags = re.MULTILINE)
        for diff in commit.diffs():
            f = diff['name']
            attr = self._parse_attr.match(git('check-attr', 'crlf', '--', f))
            if attr and attr.group('info') == 'unset':
                continue

            # Look for CRLF in the patch.
            patch = git('diff-tree', '--no-commit-id', '--root', '-p', commit.sha1, '--', f)
            if crlf.search(patch):
                failures.append('commit %s adds CRLF newlines in file\n  %s' % (commit.sha1[0:8], f))

        return failures

    def enforce(self, commit, user):
        failures = self._check_crlf(commit)
        if failures:
            fail('\n'.join(failures))
