#=============================================================================
# Copyright 2010-2016 Kitware, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#=============================================================================
from Utility import *

class CheckRobot:
    def __init__(self, path, robot):
        self._path = path
        self._robot = robot

    def enforce(self, commit, user):
        # Allow changes to path if and only if the pusher is a robot.
        for diff in commit.diffs():
            f = diff['name']
            if f.startswith(self._path) and user.email != self._robot:
                fail('commit %s not allowed: only %s may publish %s' % (commit.sha1[0:8], self._robot, str(self._path)))
            if not f.startswith(self._path) and user.email == self._robot:
                fail('commit %s not allowed: %s may publish only %s' % (commit.sha1[0:8], user, str(self._path)))

        # Accept any content from robot since upstream has its own checks.
        if user.email == self._robot:
            sys.exit(0)

